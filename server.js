const express = require('express');
const config = require('config');
const hostname = '160.153.249.245';
const app = express();
const server = require('http').createServer(app);

const io = require('socket.io')(server);
const port = process.env.PORT || config.get('app.port');

const admin = require("firebase-admin");

var serviceAccount = require("/Users/rich/Development/wmall-bfa71-firebase-adminsdk-wrop7-5cb1c1de82.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://wmall-bfa71.firebaseio.com"
});

const db = admin.firestore();
const fcm = admin.messaging();

server.listen(port, hostname, () => {
   console.log('Server listening at %s:%d', server.address().address, port);
});

io.on('connection', async (client) => {
   console.log(client.id, ' is Connected');

   client.on(config.get('chat.events.PRIVATEMESSAGE'), (data) => {
      console.log('PRIVATEMESSAGE');

      io.emit(config.get('chat.events.PRIVATEMESSAGE'), {
         'message': data
      });
   });

   client.on(config.get('signal.events.PINGRESTAURANTSTAFF'), (data) => {
      console.log('Restaurant hashid: [ ' + data['hashid'] + ' ] table number: [ ' + data['table_no'] + ' ]');

      const hashid = data['hashid'];
      const table_no = data['table_no'];

      // No validation (it will be on mobile/web) 
      io.emit(config.get('signal.events.PINGRESTAURANTSTAFF'), {
        hashid: hashid,
        table_no: table_no,
      });
   });
});
